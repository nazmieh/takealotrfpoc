*** Settings ***
Library    SeleniumLibrary
Resource    ../../Resources/Variables/GlobalVariables.robot

*** Variables ***
${LOGIN_TEXT_HEADING} =  css=body > div.rap.main.group.login-page > div > div > h2


*** Keywords ***
Load Login Page
    go to    ${LOGIN_URL}

Verify Login Page Loaded
    Sleep  5s
    element text should be   ${LOGIN_TEXT_HEADING}    Login
    capture page screenshot