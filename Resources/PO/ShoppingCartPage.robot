*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${PROCEED_TO_CHECKOUT_BUTTON} =  Proceed to Checkout
${LOGIN_HEADING} =  Login

*** Keywords ***
Click Proceed to Checkout
    click link  ${PROCEED_TO_CHECKOUT_BUTTON}
    wait until page contains  ${LOGIN_HEADING}
    capture page screenshot