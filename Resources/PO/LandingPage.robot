*** Settings ***
Library    SeleniumLibrary
Resource    ../../Resources/Variables/GlobalVariables.robot

*** Variables ***
${LOGIN_BUTTON} =  Login
${TAKEALOT_IMAGE} =  Takealot
${SEARCH_TEXTFIELD} =  xpath=//input[@class="search-field "]
${SEARCH_BUTTON} =  xpath=//button[@class="button search-btn search-icon"]


*** Keywords ***
Load Landing Page
    go to    ${STARTING_URL}
    maximize browser window

Verify Landing Page Loaded
    wait until page contains    ${LOGIN_BUTTON}
    page should contain image    ${TAKEALOT_IMAGE}
    capture page screenshot

Click Search Product
    wait until page contains    ${LOGIN_BUTTON}
    input text    ${SEARCH_TEXTFIELD}  ${SEARCH_CRITERIA}
    click button    ${SEARCH_BUTTON}
    wait until page contains    ${SEARCH_CRITERIA}
    Sleep  3s
    capture page screenshot


