*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${PRODUCT} =  xpath=//a[@href="/gigabyte-geforce-gtx-1050ti-oc-graphics-card-4gb/PLID43037510"]
${PRODUCT_HEADING} =  Gigabyte GeForce GTX 1050Ti OC Graphics Card - 4GB

*** Keywords ***
Click View Product
    sleep   3s
    click link    ${PRODUCT}
    switch window    locator=NEW
    maximize browser window
    wait until page contains  ${PRODUCT_HEADING}
    capture page screenshot