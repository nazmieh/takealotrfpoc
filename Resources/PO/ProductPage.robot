*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${ADD_TO_CART_BUTTON} =  xpath=//a[contains(@class,'expanded add-to-cart-button add-to-cart-button-module_add-to-cart-button_1a9gT')]
${ADD_TO_CART_HEADING} =  Added to cart
${GO_TO_CART_BUTTON} =  xpath=//button[@class='button checkout-now dark']
${SHOPPING_CART_HEADING} =  Shopping Cart

*** Keywords ***
Click Add to Cart
    click link    ${ADD_TO_CART_BUTTON}
    Sleep  3s
    wait until page contains   ${ADD_TO_CART_HEADING}
    capture page screenshot

Click Go to Cart
    Sleep   3s
    click button    ${GO_TO_CART_BUTTON}
    wait until page contains    ${SHOPPING_CART_HEADING}
    Sleep   3s
    capture page screenshot