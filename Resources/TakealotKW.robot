*** Settings ***
Library    SeleniumLibrary
Resource    ../Resources/PO/LoginPage.robot
Resource    ../Resources/PO/LandingPage.robot
Resource    ../Resources/PO/SearchResultsPage.robot
Resource    ../Resources/PO/ProductPage.robot
Resource    ../Resources/PO/ShoppingCartPage.robot

*** Keywords ***
Go to Login Page
    LoginPage.Load Login Page
    LoginPage.Verify Login Page Loaded

Search for a Product
    LandingPage.Load Landing Page
    LandingPage.Verify Landing Page Loaded
    LandingPage.Click Search Product

Select and View Product
    LandingPage.Load Landing Page
    LandingPage.Verify Landing Page Loaded
    LandingPage.Click Search Product
    SearchResultsPage.Click View Product

Select and Add Product
    LandingPage.Load Landing Page
    LandingPage.Verify Landing Page Loaded
    LandingPage.Click Search Product
    SearchResultsPage.Click View Product
    ProductPage.Click Add to Cart

Go to Cart
    LandingPage.Load Landing Page
    LandingPage.Verify Landing Page Loaded
    LandingPage.Click Search Product
    SearchResultsPage.Click View Product
    ProductPage.Click Add to Cart
    ProductPage.Click Go to Cart

Go to Checkout
    LandingPage.Load Landing Page
    LandingPage.Verify Landing Page Loaded
    LandingPage.Click Search Product
    SearchResultsPage.Click View Product
    ProductPage.Click Add to Cart
    ProductPage.Click Go to Cart
    ShoppingCartPage.Click Proceed to Checkout
    LoginPage.Verify Login Page Loaded



