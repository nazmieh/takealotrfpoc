*** Settings ***
Documentation    This is a Takealot POC with Page Objects

Resource    ../Resources/CommonKW.robot
Resource    ../Resources/TakealotKW.robot

Test Setup    Begin Web Test
Test Teardown    End Web Test

*** Variables ***

*** Test Cases ***
User navigates to the Login Page
    TakealotKW.Go to Login Page

User searches for a Product
    TakealotKW.Search for a Product

User views the Selected Product
    TakealotKW.Select and View Product

User adds the Product to the Cart
    TakealotKW.Select and Add Product

User proceeds to the Cart Page
    TakealotKW.Go to Cart

User proceeds to the Checkout Page
    TakealotKW.Go to Checkout